import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> someList = ['1','2','3','4','5','6','7','8'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              backgroundColor: Colors.blue,
              expandedHeight: 100.0,
floating: true,
              pinned: false,

title: Text('Title'),
              centerTitle: true,
              leading: IconButton(icon: Icon(Icons.menu), onPressed: (){}),

            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int i) {
                  return Container(
                    width: double.infinity,
                    height: 250.0,
                    color: Colors.orange,
                    margin: EdgeInsets.all(10),
                    child: Center(child: Text(someList[i])),
                  );
                },
                childCount: someList.length,
              ),
            ),
          ],
        ),
      ),

    );
  }
}
